
//atoiするためにひつよう
#include <stdlib.h>
//終端記号検索関数プロトタイプ宣言
int endcharSearch(char[]);

const int VR = 3;
void HBridgeDrive (unsigned long);
//8,9,10,11pinはハードウェアタイマー2を使用しているのでtimer2を指定
HardwareTimer timer2(2);

//LEDに出力
const int pwmOutPin = 4;
  

void setup(){
  //PWMのハードウェアタイマーの周波数変更処理
  timer2.setPeriod(100);//10kHzに設定(microseconds)
  //ここまで
  pinMode(33,OUTPUT);
  digitalWrite(33,HIGH);
  pinMode(VR,INPUT_ANALOG);
  pinMode(pwmOutPin, PWM);
  Serial1.begin(460800);
}
void loop(){
  int val = 0;
  int vals = -1;//シリアル受信値格納

  Serial1.print('C');//送信要求
  
  val = analogRead(VR);
  delayMicroseconds(500);
  
    vals = receive();
  
  if(vals >= 0){//受信ができてデータきたら駆動
    analogWrite(pwmOutPin,vals*16);
    vals = map(vals,0,4095,0,131071);
    HBridgeDrive(vals);
  }else{
    HBridgeDrive(65535);//フリー
  }
  
}





void HBridgeDrive(int DriveVal){
  //HBridge駆動関数
  //DriveVal 0～131071をうけとって、HBridgeをドライブする。
  //H1,H2はTLP591,L1,L2はTLP250 PWMはL1,L2
  //センターはフリー
  
  //pin設定
  const int H1 = 8;
  const int H2 = 9;
  const int L1 = 10;
  const int L2 = 11;
  
  pinMode(H1,OUTPUT);
  pinMode(H2,OUTPUT);
  pinMode(L1,PWM);
  pinMode(L2,PWM);
  
  int DriveCentor = 65535;
  float PWMVal = 0;
  //delayTime
  int t = 30;
  static int  DFlag;

  //センター
  if(DriveVal==DriveCentor){
    if(DFlag != 0)
    {
      //dedTime
      //全閉塞
      digitalWrite(H1,LOW);
      digitalWrite(H2,LOW);
      pwmWrite(L1,0);
      pwmWrite(L2,0);
      delay(t);
    }
    //センター駆動
    digitalWrite(H1,LOW);
    digitalWrite(H2,LOW);
    pwmWrite(L1,0);
    pwmWrite(L2,0);
    DFlag = 0;

  }
  else{
    //小側
    if(DriveVal<DriveCentor){
      PWMVal = (longmap(DriveVal,(DriveCentor - 1),0,0,1));
      //PWMVal = constrain(PWMVal,0,65535);
      
      if(DFlag != 1)
      {
        //dedTime
        //全閉塞
        digitalWrite(H1,LOW);
        digitalWrite(H2,LOW);
        pwmWrite(L1,0);
        pwmWrite(L2,0);
        delay(t);
      }
      //小側駆動
      digitalWrite(H1,HIGH);
      digitalWrite(H2,LOW);
      pwmWrite(L1,0);
      pwmWrite(L2,timer2.getOverflow() * PWMVal);
      DFlag = 1;
    }
    //大側
    if(DriveCentor<DriveVal){
      PWMVal = (longmap(DriveVal,(DriveCentor + 1),131071,0,1));
      //PWMVal = constrain(PWMVal,0,65535);
      if(DFlag != 2)
      {
        //dedTime
        //全閉塞
        digitalWrite(H1,LOW);
        digitalWrite(H2,LOW);
        pwmWrite(L1,0);
        pwmWrite(L2,0);
        delay(t);
      }
      //大側駆動
      digitalWrite(H1,LOW);
      digitalWrite(H2,HIGH);
      pwmWrite(L1,timer2.getOverflow() * PWMVal);
      pwmWrite(L2,0);
      DFlag = 2;
    }
    
  }


}



//浮動小数点型用のmap関数
float longmap(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

//受信関数
int receive (){
  int baferQty = 0;
  char receiveData [4] ;//5個
  int mainendcharposition = 0;
  char trimData [3];//4個
  int trimVal = 0; //数値化したあと。最終データ

  //シリアルバッファにデータ届いてるか確認。
  //多すぎたり少なすぎたりしたらバッファクリアして-1返すreturn
  baferQty = Serial1.available();

  if (baferQty <= 0 || baferQty >5){
    Serial1.flush();
    return -1;
  }

  //バッファにあるぶん配列に格納する。
  //配列の要素は0からはじまる。serialreadは要素があれば一つ目は1を返す。
  //よってiと配列要素の位置を合わせるならbaferQtyから-1しとくと便利(char[0-4] serial1-5)
  int i = 0;
  for(i = 0; i <= (baferQty-1); i++){
    receiveData[i] = Serial1.read();
  }

  //後ろから終端器号を搜す関数する
  mainendcharposition = endcharSearch(receiveData);

  //終端記号あればスルー、なければバッファクリアで-2返す
  if(mainendcharposition == -1){
    Serial1.flush();
    return -2;
  }

  //数字部分だけを別の配列にコピー
  int    Idx;
  for(Idx = 0;Idx < mainendcharposition;Idx++){
    trimData[Idx] = receiveData[Idx];
  }
    
  //文字列→数値キャストで変換
  trimVal = atoi(trimData);
  
  //バッファクリア,数値リターン
  Serial1.flush();
  return trimVal ;
}


//終端文字検索関数　なかったら-1 あったら場所返す(0-4)
int endcharSearch (char data[]){
  int  endcharposition = 0;
  //配列要素扱うので、最初は0
  int i = 4;
  for(i = 4;i >= 0;i--){
    //終端記号くるまで、for文まわしてifの評価繰り返して、きたら位置を記録
    if(data[i] == '#'){
      endcharposition = i;
      break;
    }
  }
  //終端器号なかったら(endcharposition変数が0のままだったら)-3返す。
  //それ以外はendcharposition返す
  if(endcharposition == 0){
    return -3;
  }
  else{
    return endcharposition;
  }

}


